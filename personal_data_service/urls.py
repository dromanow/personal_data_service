from django.conf.urls import url, include
from rest_framework import routers
from userdata.views import PersonalDataDetail, PersonalSearchView

router = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^personal_data/$', PersonalDataDetail.as_view()),
    url(r'^personal_data/(?P<uuid>[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})/$',
        PersonalDataDetail.as_view()),
    url(r'^personal_data/search/(?P<query>[_\w]+)/$', PersonalSearchView.as_view()),
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
