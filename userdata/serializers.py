from userdata.models import PersonalData
from rest_framework import serializers


class PersonalSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = PersonalData
        fields = ('id', 'first_name', 'second_name','last_name', 'email')


class SearchSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = PersonalData
        fields = ('id',)
