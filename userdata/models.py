import uuid
from django.db import models


class PersonalData(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    first_name = models.CharField('first name', max_length=255, blank=True)
    second_name = models.CharField('second name', max_length=255, blank=True)
    last_name = models.CharField('last name', max_length=255, blank=True)
    email = models.EmailField('email address', blank=True)
