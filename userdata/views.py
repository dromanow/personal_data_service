from django.http import Http404
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from userdata.models import PersonalData
from userdata.serializers import PersonalSerializer, SearchSerializer
from rest_framework import permissions


class PersonalDataDetail(APIView):

    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
    )

    def get_object(self, uuid):
        try:
            obj = PersonalData.objects.get(pk=uuid)
        except PersonalData.DoesNotExist:
            raise Http404

        return obj

    def get(self, request, uuid, format=None):
        data = self.get_object(uuid)
        data = PersonalSerializer(data)

        return Response(data.data)

    def post(self, request, format=None):
        data = request.data

        if 'id' in request.data:
            obj = self.get_object(request.data['id'])
            serializer = PersonalSerializer(obj, data=data)
        else:
            serializer = PersonalSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, uuid, format=None):
        data = self.get_object(uuid)
        data.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)


class PersonalSearchView(APIView):

    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
    )

    def get_object(self, uuid):
        try:
            obj = PersonalData.objects.get(pk=uuid)
        except PersonalData.DoesNotExist:
            raise Http404

        return obj

    def get(self, request, query, format=None):
        qf =Q(first_name__icontains=query) | Q(second_name__icontains=query) | Q(last_name__icontains=query) | Q(
            email__icontains=query)
        objects = PersonalData.objects.filter(qf)
        data = SearchSerializer(objects, many=True)

        return Response(data.data)
